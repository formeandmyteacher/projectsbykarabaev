﻿namespace Laba_1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Иванов");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Петров");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Сидоров");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Группа 7095", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3});
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Гундин");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Вертяев");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Группа 7097", new System.Windows.Forms.TreeNode[] {
            treeNode5,
            treeNode6});
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(12, 21);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "Узел1";
            treeNode1.Text = "Иванов";
            treeNode2.Name = "Узел3";
            treeNode2.Text = "Петров";
            treeNode3.Name = "Узел4";
            treeNode3.Text = "Сидоров";
            treeNode4.Name = "Узел0";
            treeNode4.Text = "Группа 7095";
            treeNode5.Name = "Узел6";
            treeNode5.Text = "Гундин";
            treeNode6.Name = "Узел7";
            treeNode6.Text = "Вертяев";
            treeNode7.Name = "Узел5";
            treeNode7.Text = "Группа 7097";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode7});
            this.treeView1.Size = new System.Drawing.Size(194, 161);
            this.treeView1.TabIndex = 0;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.treeView1);
            this.Name = "Form2";
            this.Text = "Первая форма";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
    }
}